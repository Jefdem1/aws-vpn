using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace appJeffD.VPN
{

    public class vb
    {
        public static String left(String input, int len)
        {
            return input.Substring(0, len);
        }
        public static String right(String input, int len)
        {
            return input.Substring(input.Length - len);
        }
        public static String mid(String input, int index, int len)
        {
            return input.Substring(index - 1, len);
        }
    }

    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnParse_Click(object sender, EventArgs e)
        {
            List<String> lstPSK = new List<String>();
            List<String> lstCG = new List<String>();
            List<String> lstVPG = new List<String>();
            List<String> lstCGNet = new List<String>();
            List<String> lstVPGNet = new List<String>();
            List<String> arryVPCNets = new List<String>();
            List<String> arryRemoteNets = new List<String>();
            List<String> lstASN = new List<String>();
            
            String strServerIP = txtServerIP.Text;
            String strInterfaceName = txtInterfaceName.Text;
            

            lstPSK = fnMatchWord("Pre-Shared Key           : ", 32 + 1);
            lstCG = fnMatchWord("Customer Gateway 		        : ", 15 + 1);
            lstVPG = fnMatchWord("Virtual Private Gateway	        : ", 15 + 1);
            lstCGNet = fnMatchWord("Customer Gateway         		: ", 18 + 1);
            lstASN = fnMatchWord("Customer Gateway ASN	          : ", 6 + 1);
            lstVPGNet = fnMatchWord("Virtual Private Gateway             : ", 18 + 1);

            String strASN = lstASN[0];
            


            for (int i = 0; i < lstVPCNet.Items.Count; i++)
            {
                arryVPCNets.Add(lstVPCNet.Items[i].Text);
            }
            for (int i = 0; i < lstRemoteNet.Items.Count; i++)
            {
                arryRemoteNets.Add(lstRemoteNet.Items[i].Text);
            }

            Response.Write("<br>");
            Response.Write("sudo apt-get -y install expect-lite");
            Response.Write("<br>");
            Response.Write("jeffd=$(expect -v)");
            Response.Write("<br>");
            Response.Write("if [ \"$jeffd\" != \"\" ]; then echo found; else sudo apt-get -y install expect; fi");
            Response.Write("<br>");
            Response.Write("sudo expect -c \"spawn sudo apt-get -y install racoon; sleep 1; expect \\\"*Ok*\\\"; send \\\"\\t\\r\\\"; sleep 1; expect \\\"*irec*\\\"; send \\\"\\t\\t\\r\\\";\"");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + lstVPG[0] + " " + lstPSK[0] + " >> /etc/racoon/psk.txt'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + lstVPG[1] + " " + lstPSK[1] + " >> /etc/racoon/psk.txt'");
            Response.Write("<br>");
            Response.Write("sudo chmod 600 /etc/racoon/psk.txt");
            Response.Write("<br>");
            Response.Write("sudo chmod 640 /etc/ipsec-tools.conf");
            Response.Write("<br>");


            Response.Write("sudo sh -c 'echo " + "flush\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "spdflush\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "spdadd " + lstCGNet[0] + " " + lstVPGNet[0] + " any -P out ipsec esp/tunnel/" + strServerIP + "-" + lstVPG[0] + "/require\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "spdadd " + lstVPGNet[0] + " " + lstCGNet[0] + " any -P in ipsec esp/tunnel/" + lstVPG[0] + "-" + strServerIP + "/require\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "spdadd " + lstCGNet[1] + " " + lstVPGNet[1] + " any -P out ipsec esp/tunnel/" + strServerIP + "-" + lstVPG[1] + "/require\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "spdadd " + lstVPGNet[1] + " " + lstCGNet[1] + " any -P in ipsec esp/tunnel/" + lstVPG[1] + "-" + strServerIP + "/require\\;" + " >> /etc/ipsec-tools.conf'");
            Response.Write("<br>");
            for (int i = 0; i < arryVPCNets.Count(); i++)
            {
                Response.Write("sudo sh -c 'echo " + "spdadd " + lstCGNet[0] + " " + arryVPCNets[i] + " any -P out ipsec esp/tunnel/" + strServerIP + "-" + lstVPG[0] + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                Response.Write("<br>");
                Response.Write("sudo sh -c 'echo " + "spdadd " + arryVPCNets[i] + " " + lstCGNet[0] + " any -P in ipsec esp/tunnel/" + lstVPG[0] + "-" + strServerIP + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                Response.Write("<br>");
                Response.Write("sudo sh -c 'echo " + "spdadd " + lstCGNet[1] + " " + arryVPCNets[i] + " any -P out ipsec esp/tunnel/" + strServerIP + "-" + lstVPG[0] + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                Response.Write("<br>");
                Response.Write("sudo sh -c 'echo " + "spdadd " + arryVPCNets[i] + " " + lstCGNet[1] + " any -P in ipsec esp/tunnel/" + lstVPG[0] + "-" + strServerIP + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                Response.Write("<br>");
            }
            for (int i = 0; i < arryVPCNets.Count(); i++)
            {
                for (int ii = 0; ii < arryRemoteNets.Count(); ii++)
                {
                    Response.Write("sudo sh -c 'echo " + "spdadd " + arryVPCNets[i] + " " + arryRemoteNets[ii] + " any -P out ipsec esp/tunnel/" + strServerIP + "-" + lstVPG[0] + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                    Response.Write("<br>");
                    Response.Write("sudo sh -c 'echo " + "spdadd " + arryRemoteNets[ii] + " " + arryVPCNets[i] + " any -P in ipsec esp/tunnel/" + lstVPG[0] + "-" + strServerIP + "/require\\;" + " >> /etc/ipsec-tools.conf'");
                    Response.Write("<br>");
                }
            }


          
            Response.Write("sudo sh -c 'echo " + "remote " + lstVPG[0] + " {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        exchange_mode main\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        lifetime time 28800 seconds\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        proposal {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                encryption_algorithm aes128\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                hash_algorithm sha1\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                authentication_method pre_shared_key\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                dh_group 2\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        }" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        generate_policy off\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "}" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");

            //peer 2
            Response.Write("sudo sh -c 'echo " + "remote " + lstVPG[1] + " {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        exchange_mode main\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        lifetime time 28800 seconds\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        proposal {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                encryption_algorithm aes128\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                hash_algorithm sha1\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                authentication_method pre_shared_key\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "                dh_group 2\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        }" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "        generate_policy off\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "}" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");

            //sainfo 1
            Response.Write("sudo sh -c 'echo " + "sainfo address " + lstCGNet[0] + " any address " + lstVPGNet[0] + " any {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    pfs_group 2\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    lifetime time 3600 seconds\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    encryption_algorithm aes128\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    authentication_algorithm hmac_sha1\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    compression_algorithm deflate\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "}" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");

            //sainfo 2
            Response.Write("sudo sh -c 'echo " + "sainfo address " + lstCGNet[1] + " any address " + lstVPGNet[1] + " any {" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    pfs_group 2\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    lifetime time 3600 seconds\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    encryption_algorithm aes128\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    authentication_algorithm hmac_sha1\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "    compression_algorithm deflate\\;" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "}" + " >> /etc/racoon/racoon.conf'");
            Response.Write("<br>");
            Response.Write("<br>");


            //create interfaces
            Response.Write("sudo sh -c 'sed -i s/\"iface " + strInterfaceName + "\"/\"#iface " + strInterfaceName + "\"/ /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "iface " + strInterfaceName + " inet manual >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "auto " + strInterfaceName + ":0 >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "iface " + strInterfaceName + ":0 inet static >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "address " + vb.left(lstCGNet[0], (lstCGNet[0].ToString().Count() - 3)) + " >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "netmask 255.255.255.252 >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "auto " + strInterfaceName + ":1 >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "iface " + strInterfaceName + ":1 inet static >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "address " + vb.left(lstCGNet[1], (lstCGNet[1].ToString().Count() - 3)) + " >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo " + "netmask 255.255.255.252 >> /etc/network/interfaces'");
            Response.Write("<br>");

            /*
            Response.Write("sudo ip a a " + lstCGNet[0] + " dev " + strInterfaceName);
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo up ip addr add " + lstCGNet[0] + " dev " + strInterfaceName + " >> /etc/network/interfaces'");
            Response.Write("<br>");
            Response.Write("sudo ip a a " + lstCGNet[1] + " dev " + strInterfaceName);
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo up ip addr add " + lstCGNet[1] + " dev " + strInterfaceName + " >> /etc/network/interfaces'");
            Response.Write("<br>");
            */

            Response.Write("<br>");
            Response.Write("sudo service networking restart");
            Response.Write("<br>");
            Response.Write("sudo /etc/init.d/racoon start");
            Response.Write("<br>");
            Response.Write("sudo /etc/init.d/setkey start");
            Response.Write("<br>");
            Response.Write("ping -c 3 " + vb.left(lstVPGNet[0], (lstVPGNet[0].ToString().Count() - 3)) + "; ping -c 3 " + vb.left(lstVPGNet[1], (lstVPGNet[1].ToString().Count() - 3)));
            Response.Write("<br>");

            Response.Write("");
            Response.Write("<br>");
            Response.Write("");
            Response.Write("<br>");
            Response.Write("");
            Response.Write("<br>");
            Response.Write("");
            Response.Write("<br>");




            Response.Write("sudo sysctl -w net.ipv4.ip_forward=1");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo net.ipv4.ip_forward = 1 >> /etc/sysctl.conf'");
            Response.Write("<br>");
            Response.Write("sudo apt-get -y install quagga ");
            Response.Write("<br>");
            Response.Write("sudo touch /etc/quagga/zebra.conf");
            Response.Write("<br>");
            Response.Write("sudo chown quagga.quagga /etc/quagga/zebra.conf");
            Response.Write("<br>");
            Response.Write("sudo chmod 640 /etc/quagga/zebra.conf");
            Response.Write("<br>");
            Response.Write("sudo touch /etc/quagga/bgpd.conf");
            Response.Write("<br>");
            Response.Write("sudo chown quagga.quagga /etc/quagga/bgpd.conf");
            Response.Write("<br>");
            Response.Write("sudo chmod 640 /etc/quagga/bgpd.conf");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo hostname ubuntu >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo password a >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo enable password a >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo log file /var/log/quagga/bgpd >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo !debug bgp events >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo !debug bgp zebra >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo debug bgp updates >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo router bgp " + strASN + " >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo bgp router-id " + strServerIP + " >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");

            Response.Write("sudo sh -c 'echo network " + fnGetNetwork(lstVPGNet[0]) + " >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo network " + fnGetNetwork(lstVPGNet[1]) + " >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            for (int i = 0; i < arryRemoteNets.Count(); i++)
            {
                Response.Write("sudo sh -c 'echo network " + arryRemoteNets[i] + " >> /etc/quagga/bgpd.conf'");
                Response.Write("<br>");
            }
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! aws tunnel #1 neighbour >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo neighbor " + lstVPGNet[0] + " remote-as 7224 >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! aws tunnel #2 neighbour >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo neighbor " + lstVPGNet[1] + " remote-as 7224 >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo line vty >> /etc/quagga/bgpd.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo hostname ubuntu >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo password a >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo enable password a >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! list interfaces >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo interface " + strInterfaceName + " >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo interface lo >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo ! >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'echo line vty >> /etc/quagga/zebra.conf'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'sed -i 's/bgpd=no/bgpd=yes/' /etc/quagga/daemons'");
            Response.Write("<br>");
            Response.Write("sudo sh -c 'sed -i 's/zebra=no/zebra=yes/' /etc/quagga/daemons'");
            
            Response.Write("<br>");
            Response.Write("sudo /etc/init.d/quagga start");
            Response.Write("<br>");
            Response.Write("<br>");
            Response.Write("<br>");
            Response.Write("<br>");
            Response.Write("<br>");
            Response.Write("Helpful Info");
            Response.Write("<br>");
            Response.Write("sudo racoonctl show-sa ipsec");
            Response.Write("<br>");
            Response.Write("sudo tail -f /var/log/syslog");
            Response.Write("<br>");
            Response.Write("make sure to enable route propogation in VPC");
            Response.Write("<br>");
            Response.Write("double check UFW firewall on linux server, not sure if this is needed.");
            Response.Write("<br>");
            Response.Write("<br>");
            Response.Write("<br>");

            Response.Write("Virtual Private Gateway 1: ");
            Response.Write(lstVPG[0]);
            Response.Write("<br>");
            Response.Write("Virtual Private Gateway 2: ");
            Response.Write(lstVPG[1]);
            Response.Write("<br>");
            Response.Write("Customer Gateway 1: ");
            Response.Write(lstCG[0]);
            Response.Write("<br>");
            Response.Write("Customer Gateway 2: ");
            Response.Write(lstCG[1]);
            Response.Write("<br>");

            Response.Write("Tunnel Interface Client Side 1: ");
            Response.Write(lstCGNet[0]);
            Response.Write("<br>");
            Response.Write("Tunnel Interface Client Side 2: ");
            Response.Write(lstCGNet[1]);
            Response.Write("<br>");
            Response.Write("Tunnel Interface AWS Side 1: ");
            Response.Write(lstVPGNet[0]);
            Response.Write("<br>");
            Response.Write("Tunnel Interface AWS Side 2: ");
            Response.Write(lstVPGNet[1]);
            Response.Write("<br>");
            Response.Write("<br>");
        }

        List<string> fnMatchWord(string strMatchWord, int intMaxLength)
        {
            List<string> strResults = new List<string>();
            string strWhiteSpace = "";
            string strAdd = "";
            for (int i = 1; i < txtConfig.Text.Count() - (strMatchWord.Count() + 1); i++)
            {
                if (vb.mid(txtConfig.Text, i, strMatchWord.Count()) == strMatchWord)
                {
                    for (int ii = 0; ii < intMaxLength; ii++)
                    {
                        strWhiteSpace = vb.mid(txtConfig.Text, i + ii + strMatchWord.Count(), 1);
                        if (strWhiteSpace == "\r")
                        {
                            strAdd = vb.mid(txtConfig.Text, i + strMatchWord.Count(), ii);
                            strResults.Add(strAdd);
                            strWhiteSpace = "";
                            strAdd = "";
                            ii = intMaxLength;
                        }
                    }

                }

            }
            return strResults;
        }

        String fnGetNetwork(string strNetwork)
        {
            String strIP = vb.left(strNetwork, (strNetwork.Count() - 3));
            String strInt = vb.right(strIP, 1);
            String strNewString = vb.left(strIP, strIP.Count() - 1);
            Int32 intIP = Convert.ToInt32(strInt);
            if (intIP == 0)
            {
                intIP = 9;
            }
            else
            {
                intIP = intIP - 1;
            }

            strNewString = strNewString + intIP + "/30";
            return strNewString;
        }

        protected void lbAddVPCNet_Click(object sender, EventArgs e)
        {
            lstVPCNet.Items.Add(txtVPCNet.Text);
            txtVPCNet.Text = "";
        }

        protected void lbAddRemoteNet_Click(object sender, EventArgs e)
        {
            lstRemoteNet.Items.Add(txtRemoteNet.Text);
            txtRemoteNet.Text = "";
        }
    }
}
