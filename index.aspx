<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="appJeffD.VPN.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
    <asp:Label ID="lblServerIP" runat="server" Text="Racoon Server Interface IP: "></asp:Label>
        <asp:TextBox ID="txtServerIP" runat="server">192.168.1.151</asp:TextBox>
        <br />
        <asp:Label ID="lblInterfaceName" runat="server" Text="Racoon Server Interface Name: "></asp:Label>
        <asp:TextBox ID="txtInterfaceName" runat="server">eno16780032</asp:TextBox>
        <br />
        <asp:ListBox ID="lstVPCNet" runat="server" Width="130px"></asp:ListBox>
        <asp:ListBox ID="lstRemoteNet" runat="server" Width="130px"></asp:ListBox>
        <br />
        <asp:TextBox ID="txtVPCNet" runat="server" Width="130px">172.31.0.0/16</asp:TextBox>
        <asp:TextBox ID="txtRemoteNet" runat="server" Width="130px">192.168.1.0/24</asp:TextBox>
        <br />
        <asp:LinkButton ID="lbAddVPCNet" runat="server" Width="130px" OnClick="lbAddVPCNet_Click">Add VPC Net</asp:LinkButton>
        <asp:LinkButton ID="lbAddRemoteNet" runat="server" Width="130px" OnClick="lbAddRemoteNet_Click">Add Remote Net</asp:LinkButton>
        <br />
        <asp:TextBox ID="txtConfig" runat="server" Height="242px" TextMode="MultiLine" Width="409px"></asp:TextBox>
        <br />
        <asp:Button ID="btnParse" runat="server" Text="Parse" OnClick="btnParse_Click" />
    </div>
    </form>
</body>
</html>
